package com.etnetera.hr.service;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class JavaScriptFrameworkService {

    private final JavaScriptFrameworkRepository repository;

    @Autowired
    public JavaScriptFrameworkService(JavaScriptFrameworkRepository repository) {
        this.repository = repository;
    }

    public Iterable<JavaScriptFramework> getFrameworks(){
        return repository.findAll();
    }

    public void addNew(JavaScriptFramework framework){
        repository.save(framework);
    }

    public void update(JavaScriptFramework framework){
        Optional<JavaScriptFramework> toUpdate = repository.findById(framework.getId());
        if(toUpdate.isEmpty()){
            throw new RuntimeException();
        }
        toUpdate.get().setName(framework.getName());
        toUpdate.get().setDeprecationDate(framework.getDeprecationDate());
        toUpdate.get().setVersions(framework.getVersions());
        toUpdate.get().setHypeLevel(framework.getHypeLevel());

        repository.save(toUpdate.get());
    }

    public void delete(long id){
        Optional<JavaScriptFramework> toDelete = repository.findById(id);
        if(toDelete.isEmpty()){
            throw new RuntimeException();
        }
        repository.delete(toDelete.get());
    }

    public List<JavaScriptFramework> findByName(String name){
        return repository.getFrameworksByName(name);
    }
}
