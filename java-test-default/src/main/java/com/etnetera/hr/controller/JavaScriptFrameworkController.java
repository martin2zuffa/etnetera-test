package com.etnetera.hr.controller;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.service.JavaScriptFrameworkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Simple REST controller for accessing application logic.
 * 
 * @author Etnetera
 *
 */
@RestController
public class JavaScriptFrameworkController {

	private final JavaScriptFrameworkService service;

	@Autowired
	public JavaScriptFrameworkController(JavaScriptFrameworkService service) {
		this.service = service;
	}

	@GetMapping("/frameworks")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Iterable<JavaScriptFramework> frameworks() {
		return service.getFrameworks();
	}

	@PostMapping("/add_framework")
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	public void addFramework(@RequestBody JavaScriptFramework framework){
		service.addNew(framework);
	}

	@PutMapping("/update_framework")
	@ResponseStatus(HttpStatus.CREATED)
	public void updateFramework(@RequestBody JavaScriptFramework framework){
		service.update(framework);
	}

	@DeleteMapping("/delete_framework")
	@ResponseStatus(HttpStatus.OK)
	public void deleteFramework(@RequestParam long id){
		service.delete(id);
	}

	@GetMapping("/frameworks/name")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<JavaScriptFramework> findFrameworkByName(@RequestParam String name){
		return service.findByName(name);
	}

}
