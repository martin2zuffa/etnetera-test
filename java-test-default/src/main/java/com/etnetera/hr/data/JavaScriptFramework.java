package com.etnetera.hr.data;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Simple data entity describing basic properties of every JavaScript framework.
 * 
 * @author Etnetera
 *
 */
@Data
@Entity(name = "javascript_framework")
public class JavaScriptFramework {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(nullable = false, length = 30)
	private String name;

	@ElementCollection
	@CollectionTable(name = "framework_version", joinColumns = @JoinColumn(name = "javascriptframework_id"))
	@Column(name = "version")
	private List<String> versions = new ArrayList<>();

	@Column
	private LocalDate deprecationDate;

	@Column
	private double hypeLevel;

	public JavaScriptFramework() {
	}

	public JavaScriptFramework(String name) {
		this.name = name;
	}
}
