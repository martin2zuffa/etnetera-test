package com.etnetera.hr.data;

import lombok.Data;

import javax.persistence.*;

@Data
@Embeddable
public class FrameworkVersion {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, length = 30)
    private String version;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "javascriptframework_id")
    private JavaScriptFramework framework;

}
