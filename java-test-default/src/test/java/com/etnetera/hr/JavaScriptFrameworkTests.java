package com.etnetera.hr;

import com.etnetera.hr.controller.JavaScriptFrameworkController;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import com.etnetera.hr.service.JavaScriptFrameworkService;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Class used for Spring Boot/MVC based tests.
 * 
 * @author Etnetera
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles("test")
@Transactional
public class JavaScriptFrameworkTests {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private JavaScriptFrameworkController controller;

    @Autowired
    private JavaScriptFrameworkService service;

    @Autowired
    private JavaScriptFrameworkRepository repository;


    @Test
    public void test()throws Exception{

        final String insertedEntity = "{\"name\": \"test\", \"versions\": [\"1.0\"], \"deprecationDate\": \"2019-12-31\", \"hypeLevel\": 1.2}";
        final String updatedEntity = "{\"id\": 1, \"name\": \"test\", \"versions\": [\"1.0\",\"1.1\"], \"deprecationDate\": \"2019-12-31\", \"hypeLevel\": 1.2}";
        final String inserteCheckEntity = "[{\"id\":1,\"name\":\"test\",\"versions\":[\"1.0\"],\"deprecationDate\":\"2019-12-31\",\"hypeLevel\":1.2}]";
        final String updatedCheckEntity = "[{\"id\": 1, \"name\": \"test\", \"versions\": [\"1.0\",\"1.1\"], \"deprecationDate\": \"2019-12-31\", \"hypeLevel\": 1.2}]";

        mvc.perform(post("/add_framework")
                .content(insertedEntity)
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        String insertResult = mvc.perform(get("/frameworks"))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        Assert.isTrue(insertResult.trim().equals(inserteCheckEntity.trim()),"Error result do not match");

        mvc.perform(put("/update_framework")
                .content(updatedEntity)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

         mvc.perform(get("/frameworks"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                 .andExpect(jsonPath("$[0].versions", Matchers.is(List.of("1.0","1.1"))));

        mvc.perform(get("/frameworks/name")
                    .contentType(MediaType.MULTIPART_FORM_DATA)
                    .param("name","test"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].name", Matchers.is("test")));

        mvc.perform(delete("/delete_framework")
                    .contentType(MediaType.MULTIPART_FORM_DATA)
                    .param("id","1"))
                .andExpect(status().isOk());

        mvc.perform(get("/frameworks"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", Matchers.empty()));

    }

}
